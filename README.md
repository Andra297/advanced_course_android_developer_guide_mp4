# Advanced Android Developer Guide

# About
I put together a video series for beginners and intermediate users who wanted to compile Android for themselves. However, I received a lot of comments asking for more advanced building techniques, so here is my attempt to add some more advanced topics! I will warn you, I am not a very advanced Android developer, so I don't know how much advanced material will be here, but for what it is worth, here is what I know. :D

(You can find the beginner courses at https://thealaskalinuxuser.wordpress.com or at https://gitlab.com/alaskalinuxuser/course_android_developer_guide or https://gitlab.com/alaskalinuxuser/course_android_developer_guide_mp4).

# Who is this course for?
This course is for those who are able to build custom roms and kernels for devices that already have device trees. E.g., a 'rom cooker' who has built a rom or two for phones that already had custom roms, and now want to take it to the next level. IF YOU HAVE NEVER BUILT CUSTOM ROMS BEFORE, PLEASE START WITH THE PREVIOUS (non advanced) VIDEO SERIES!

